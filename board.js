
var result = '';
for (var h = 0; h < 8; h++) {
    for (var w = 0; w < 8; w++) {
        if (h % 2 == 0 && w % 2 == 0 || h % 2 !== 0 && w % 2 !== 0) {
            result += '  ';
        } else {
            result += '██';
        }
    }
    result += '\n';
}
console.log(result);
